EESchema Schematic File Version 2
LIBS:riaa_preamp-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:riaa_preamp-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L TL072-RESCUE-riaa_preamp U1
U 1 1 55D72DE6
P 4850 1700
F 0 "U1" H 4800 1900 60  0000 L CNN
F 1 "TL072" H 4800 1450 60  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_LongPads" H 4850 1700 60  0001 C CNN
F 3 "" H 4850 1700 60  0000 C CNN
	1    4850 1700
	1    0    0    -1  
$EndComp
$Comp
L TL072-RESCUE-riaa_preamp U2
U 1 1 55D730B9
P 6300 1700
F 0 "U2" H 6250 1900 60  0000 L CNN
F 1 "TL072" H 6250 1450 60  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_LongPads" H 6300 1700 60  0001 C CNN
F 3 "" H 6300 1700 60  0000 C CNN
	1    6300 1700
	1    0    0    -1  
$EndComp
$Comp
L R-RESCUE-riaa_preamp R1
U 1 1 55D7687D
P 3300 2250
F 0 "R1" V 3380 2250 40  0000 C CNN
F 1 "47k" V 3307 2251 40  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 3230 2250 30  0001 C CNN
F 3 "" H 3300 2250 30  0000 C CNN
	1    3300 2250
	1    0    0    -1  
$EndComp
$Comp
L R-RESCUE-riaa_preamp R5
U 1 1 55D768D4
P 3950 1600
F 0 "R5" V 4030 1600 40  0000 C CNN
F 1 "2.2k" V 3957 1601 40  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 3880 1600 30  0001 C CNN
F 3 "" H 3950 1600 30  0000 C CNN
	1    3950 1600
	0    1    1    0   
$EndComp
$Comp
L R-RESCUE-riaa_preamp R3
U 1 1 55D7694D
P 3650 2050
F 0 "R3" V 3730 2050 40  0000 C CNN
F 1 "4.7k" V 3657 2051 40  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 3580 2050 30  0001 C CNN
F 3 "" H 3650 2050 30  0000 C CNN
	1    3650 2050
	1    0    0    -1  
$EndComp
$Comp
L CP1-RESCUE-riaa_preamp C1
U 1 1 55D76AB2
P 3650 2650
F 0 "C1" H 3700 2750 50  0000 L CNN
F 1 "22u" H 3700 2550 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Radial_D5_L11_P2.5" H 3650 2650 60  0001 C CNN
F 3 "" H 3650 2650 60  0000 C CNN
	1    3650 2650
	-1   0    0    1   
$EndComp
Wire Wire Line
	2950 1600 3700 1600
Wire Wire Line
	3300 2000 3300 1600
Connection ~ 3300 1600
Wire Wire Line
	3650 1800 4350 1800
Wire Wire Line
	3650 2300 3650 2450
Wire Wire Line
	3300 2500 3300 2950
Wire Wire Line
	3650 2850 3650 3100
Connection ~ 3300 2950
Wire Wire Line
	4200 1600 4350 1600
$Comp
L R-RESCUE-riaa_preamp R7
U 1 1 55D77588
P 4400 2300
F 0 "R7" V 4480 2300 40  0000 C CNN
F 1 "180k" V 4407 2301 40  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 4330 2300 30  0001 C CNN
F 3 "" H 4400 2300 30  0000 C CNN
	1    4400 2300
	0    1    1    0   
$EndComp
$Comp
L C-RESCUE-riaa_preamp C3
U 1 1 55D775EB
P 4400 2600
F 0 "C3" H 4400 2700 40  0000 L CNN
F 1 "22n" H 4406 2515 40  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D7.5_P5" H 4438 2450 30  0001 C CNN
F 3 "" H 4400 2600 60  0000 C CNN
	1    4400 2600
	0    1    1    0   
$EndComp
$Comp
L R-RESCUE-riaa_preamp R9
U 1 1 55D7766A
P 5100 2600
F 0 "R9" V 5180 2600 40  0000 C CNN
F 1 "10k" V 5107 2601 40  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 5030 2600 30  0001 C CNN
F 3 "" H 5100 2600 30  0000 C CNN
	1    5100 2600
	0    1    1    0   
$EndComp
Wire Wire Line
	4200 2600 4050 2600
Wire Wire Line
	4050 2600 4050 1800
Connection ~ 4050 1800
Connection ~ 4050 2300
Wire Wire Line
	5350 1600 5350 2600
Connection ~ 5350 2300
Wire Wire Line
	4050 2300 4150 2300
Wire Wire Line
	4650 2300 5350 2300
Wire Wire Line
	4850 2600 4600 2600
$Comp
L R-RESCUE-riaa_preamp R11
U 1 1 55D7900D
P 5600 2050
F 0 "R11" V 5680 2050 40  0000 C CNN
F 1 "2.7k" V 5607 2051 40  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 5530 2050 30  0001 C CNN
F 3 "" H 5600 2050 30  0000 C CNN
	1    5600 2050
	1    0    0    -1  
$EndComp
$Comp
L CP1-RESCUE-riaa_preamp C5
U 1 1 55D7929E
P 5600 2600
F 0 "C5" H 5650 2700 50  0000 L CNN
F 1 "22u" H 5650 2500 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Radial_D5_L11_P2.5" H 5600 2600 60  0001 C CNN
F 3 "" H 5600 2600 60  0000 C CNN
	1    5600 2600
	-1   0    0    1   
$EndComp
Wire Wire Line
	5350 1600 5800 1600
Connection ~ 5350 1700
Wire Wire Line
	5600 2950 5600 2800
Connection ~ 3650 2950
Wire Wire Line
	5600 2400 5600 2300
Wire Wire Line
	5600 1800 5800 1800
$Comp
L R-RESCUE-riaa_preamp R13
U 1 1 55D7A424
P 6650 2400
F 0 "R13" V 6730 2400 40  0000 C CNN
F 1 "100k" V 6657 2401 40  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 6580 2400 30  0001 C CNN
F 3 "" H 6650 2400 30  0000 C CNN
	1    6650 2400
	0    1    1    0   
$EndComp
Wire Wire Line
	6400 2400 5800 2400
Wire Wire Line
	5800 2400 5800 1800
Connection ~ 5800 1800
Wire Wire Line
	6900 2400 7000 2400
Wire Wire Line
	7000 2400 7000 1700
Wire Wire Line
	6800 1700 7100 1700
$Comp
L R-RESCUE-riaa_preamp R15
U 1 1 55D7A6E6
P 7350 1700
F 0 "R15" V 7430 1700 40  0000 C CNN
F 1 "820" V 7357 1701 40  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 7280 1700 30  0001 C CNN
F 3 "" H 7350 1700 30  0000 C CNN
	1    7350 1700
	0    1    1    0   
$EndComp
Connection ~ 7000 1700
$Comp
L C-RESCUE-riaa_preamp C9
U 1 1 55D7AC6D
P 8000 1700
F 0 "C9" H 8000 1800 40  0000 L CNN
F 1 "1u" H 8006 1615 40  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D7.5_P5" H 8038 1550 30  0001 C CNN
F 3 "" H 8000 1700 60  0000 C CNN
	1    8000 1700
	0    1    1    0   
$EndComp
$Comp
L C-RESCUE-riaa_preamp C7
U 1 1 55D7ACBE
P 7700 2250
F 0 "C7" H 7700 2350 40  0000 L CNN
F 1 "82n" H 7706 2165 40  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D7.5_P5" H 7738 2100 30  0001 C CNN
F 3 "" H 7700 2250 60  0000 C CNN
	1    7700 2250
	1    0    0    -1  
$EndComp
$Comp
L R-RESCUE-riaa_preamp R17
U 1 1 55D7AFD0
P 8350 2250
F 0 "R17" V 8430 2250 40  0000 C CNN
F 1 "100k" V 8357 2251 40  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 8280 2250 30  0001 C CNN
F 3 "" H 8350 2250 30  0000 C CNN
	1    8350 2250
	1    0    0    -1  
$EndComp
Connection ~ 5600 2950
Wire Wire Line
	7700 2450 7700 2950
Connection ~ 7700 2950
Wire Wire Line
	8350 2950 8350 2500
Connection ~ 8350 2950
Wire Wire Line
	7700 2050 7700 1700
Wire Wire Line
	7600 1700 7800 1700
Connection ~ 7700 1700
Wire Wire Line
	8200 1700 10100 1700
Wire Wire Line
	8350 2000 8350 1700
Connection ~ 8350 1700
$Comp
L R-RESCUE-riaa_preamp R2
U 1 1 55D813B7
P 3350 4550
F 0 "R2" V 3430 4550 40  0000 C CNN
F 1 "47k" V 3357 4551 40  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 3280 4550 30  0001 C CNN
F 3 "" H 3350 4550 30  0000 C CNN
	1    3350 4550
	1    0    0    -1  
$EndComp
$Comp
L R-RESCUE-riaa_preamp R6
U 1 1 55D813BD
P 4000 3900
F 0 "R6" V 4080 3900 40  0000 C CNN
F 1 "2.2k" V 4007 3901 40  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 3930 3900 30  0001 C CNN
F 3 "" H 4000 3900 30  0000 C CNN
	1    4000 3900
	0    1    1    0   
$EndComp
$Comp
L R-RESCUE-riaa_preamp R4
U 1 1 55D813C3
P 3700 4350
F 0 "R4" V 3780 4350 40  0000 C CNN
F 1 "4.7k" V 3707 4351 40  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 3630 4350 30  0001 C CNN
F 3 "" H 3700 4350 30  0000 C CNN
	1    3700 4350
	1    0    0    -1  
$EndComp
$Comp
L CP1-RESCUE-riaa_preamp C2
U 1 1 55D813C9
P 3700 4950
F 0 "C2" H 3750 5050 50  0000 L CNN
F 1 "22u" H 3750 4850 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Radial_D5_L11_P2.5" H 3700 4950 60  0001 C CNN
F 3 "" H 3700 4950 60  0000 C CNN
	1    3700 4950
	-1   0    0    1   
$EndComp
Wire Wire Line
	3350 3500 3350 4300
Connection ~ 3350 3900
Wire Wire Line
	3700 4100 4400 4100
Wire Wire Line
	3700 4600 3700 4750
Wire Wire Line
	3350 4800 3350 5250
Wire Wire Line
	3700 5150 3700 5400
Connection ~ 3350 5250
Wire Wire Line
	4250 3900 4400 3900
$Comp
L R-RESCUE-riaa_preamp R8
U 1 1 55D813D9
P 4450 4600
F 0 "R8" V 4530 4600 40  0000 C CNN
F 1 "180k" V 4457 4601 40  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 4380 4600 30  0001 C CNN
F 3 "" H 4450 4600 30  0000 C CNN
	1    4450 4600
	0    1    1    0   
$EndComp
$Comp
L C-RESCUE-riaa_preamp C4
U 1 1 55D813DF
P 4450 4900
F 0 "C4" H 4450 5000 40  0000 L CNN
F 1 "22n" H 4456 4815 40  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D7.5_P5" H 4488 4750 30  0001 C CNN
F 3 "" H 4450 4900 60  0000 C CNN
	1    4450 4900
	0    1    1    0   
$EndComp
$Comp
L R-RESCUE-riaa_preamp R10
U 1 1 55D813E5
P 5150 4900
F 0 "R10" V 5230 4900 40  0000 C CNN
F 1 "10k" V 5157 4901 40  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 5080 4900 30  0001 C CNN
F 3 "" H 5150 4900 30  0000 C CNN
	1    5150 4900
	0    1    1    0   
$EndComp
Wire Wire Line
	4250 4900 4100 4900
Wire Wire Line
	4100 4900 4100 4100
Connection ~ 4100 4100
Connection ~ 4100 4600
Wire Wire Line
	5400 3900 5400 4900
Connection ~ 5400 4600
Wire Wire Line
	4100 4600 4200 4600
Wire Wire Line
	4700 4600 5400 4600
Wire Wire Line
	4900 4900 4650 4900
$Comp
L R-RESCUE-riaa_preamp R12
U 1 1 55D813F4
P 5650 4350
F 0 "R12" V 5730 4350 40  0000 C CNN
F 1 "2.7k" V 5657 4351 40  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 5580 4350 30  0001 C CNN
F 3 "" H 5650 4350 30  0000 C CNN
	1    5650 4350
	1    0    0    -1  
$EndComp
$Comp
L CP1-RESCUE-riaa_preamp C6
U 1 1 55D813FA
P 5650 4900
F 0 "C6" H 5700 5000 50  0000 L CNN
F 1 "22u" H 5700 4800 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Radial_D5_L11_P2.5" H 5650 4900 60  0001 C CNN
F 3 "" H 5650 4900 60  0000 C CNN
	1    5650 4900
	-1   0    0    1   
$EndComp
Wire Wire Line
	5400 3900 5850 3900
Connection ~ 5400 4000
Wire Wire Line
	5650 5250 5650 5100
Connection ~ 3700 5250
Wire Wire Line
	5650 4700 5650 4600
Wire Wire Line
	5650 4100 5850 4100
$Comp
L R-RESCUE-riaa_preamp R14
U 1 1 55D81406
P 6700 4700
F 0 "R14" V 6780 4700 40  0000 C CNN
F 1 "100k" V 6707 4701 40  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 6630 4700 30  0001 C CNN
F 3 "" H 6700 4700 30  0000 C CNN
	1    6700 4700
	0    1    1    0   
$EndComp
Wire Wire Line
	6450 4700 5850 4700
Wire Wire Line
	5850 4700 5850 4100
Connection ~ 5850 4100
Wire Wire Line
	6950 4700 7050 4700
Wire Wire Line
	7050 4700 7050 4000
Wire Wire Line
	6850 4000 7150 4000
$Comp
L R-RESCUE-riaa_preamp R16
U 1 1 55D81412
P 7400 4000
F 0 "R16" V 7480 4000 40  0000 C CNN
F 1 "820" V 7407 4001 40  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 7330 4000 30  0001 C CNN
F 3 "" H 7400 4000 30  0000 C CNN
	1    7400 4000
	0    1    1    0   
$EndComp
Connection ~ 7050 4000
$Comp
L C-RESCUE-riaa_preamp C10
U 1 1 55D81419
P 8050 4000
F 0 "C10" H 8050 4100 40  0000 L CNN
F 1 "1u" H 8056 3915 40  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D7.5_P5" H 8088 3850 30  0001 C CNN
F 3 "" H 8050 4000 60  0000 C CNN
	1    8050 4000
	0    1    1    0   
$EndComp
$Comp
L C-RESCUE-riaa_preamp C8
U 1 1 55D8141F
P 7750 4550
F 0 "C8" H 7750 4650 40  0000 L CNN
F 1 "82n" H 7756 4465 40  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D7.5_P5" H 7788 4400 30  0001 C CNN
F 3 "" H 7750 4550 60  0000 C CNN
	1    7750 4550
	1    0    0    -1  
$EndComp
$Comp
L R-RESCUE-riaa_preamp R18
U 1 1 55D81425
P 8400 4550
F 0 "R18" V 8480 4550 40  0000 C CNN
F 1 "100k" V 8407 4551 40  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 8330 4550 30  0001 C CNN
F 3 "" H 8400 4550 30  0000 C CNN
	1    8400 4550
	1    0    0    -1  
$EndComp
Connection ~ 5650 5250
Wire Wire Line
	7750 4750 7750 5250
Connection ~ 7750 5250
Wire Wire Line
	8400 4800 8400 5250
Connection ~ 8400 5250
Wire Wire Line
	7750 4350 7750 4000
Wire Wire Line
	7650 4000 7850 4000
Connection ~ 7750 4000
Wire Wire Line
	8250 4000 10100 4000
Wire Wire Line
	8400 4300 8400 4000
Connection ~ 8400 4000
$Comp
L TL072-RESCUE-riaa_preamp U1
U 2 1 55D84D55
P 4900 4000
F 0 "U1" H 4850 4200 60  0000 L CNN
F 1 "TL072" H 4850 3750 60  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_LongPads" H 4900 4000 60  0001 C CNN
F 3 "" H 4900 4000 60  0000 C CNN
	2    4900 4000
	1    0    0    -1  
$EndComp
$Comp
L TL072-RESCUE-riaa_preamp U2
U 2 1 55D84DF8
P 6350 4000
F 0 "U2" H 6300 4200 60  0000 L CNN
F 1 "TL072" H 6300 3750 60  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_LongPads" H 6350 4000 60  0001 C CNN
F 3 "" H 6350 4000 60  0000 C CNN
	2    6350 4000
	1    0    0    -1  
$EndComp
$Comp
L CONN_3 K1
U 1 1 55D86497
P 1700 6600
F 0 "K1" V 1650 6600 50  0000 C CNN
F 1 "POWER" V 1750 6600 40  0000 C CNN
F 2 "Connect:bornier3" H 1700 6600 60  0001 C CNN
F 3 "" H 1700 6600 60  0000 C CNN
	1    1700 6600
	-1   0    0    1   
$EndComp
$Comp
L GND-RESCUE-riaa_preamp #PWR01
U 1 1 55D86BD8
P 3700 5400
F 0 "#PWR01" H 3700 5400 30  0001 C CNN
F 1 "GND" H 3700 5330 30  0001 C CNN
F 2 "" H 3700 5400 60  0000 C CNN
F 3 "" H 3700 5400 60  0000 C CNN
	1    3700 5400
	1    0    0    -1  
$EndComp
$Comp
L GND-RESCUE-riaa_preamp #PWR02
U 1 1 55D87135
P 3650 3100
F 0 "#PWR02" H 3650 3100 30  0001 C CNN
F 1 "GND" H 3650 3030 30  0001 C CNN
F 2 "" H 3650 3100 60  0000 C CNN
F 3 "" H 3650 3100 60  0000 C CNN
	1    3650 3100
	1    0    0    -1  
$EndComp
$Comp
L GND-RESCUE-riaa_preamp #PWR03
U 1 1 55D89D37
P 2550 6950
F 0 "#PWR03" H 2550 6950 30  0001 C CNN
F 1 "GND" H 2550 6880 30  0001 C CNN
F 2 "" H 2550 6950 60  0000 C CNN
F 3 "" H 2550 6950 60  0000 C CNN
	1    2550 6950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 6600 2550 6600
Wire Wire Line
	2550 6600 2550 6950
Text GLabel 2050 6700 2    60   Input ~ 0
-15V
Text GLabel 2050 6500 2    60   Input ~ 0
+15V
Text GLabel 4750 1300 0    60   Input ~ 0
+15V
Text GLabel 4750 2100 0    60   Input ~ 0
-15V
Text GLabel 6200 1300 0    60   Input ~ 0
+15V
Text GLabel 4800 3600 0    60   Input ~ 0
+15V
Text GLabel 6250 3600 0    60   Input ~ 0
+15V
Text GLabel 6200 2100 0    60   Input ~ 0
-15V
Text GLabel 4800 4400 0    60   Input ~ 0
-15V
Text GLabel 6250 4400 0    60   Input ~ 0
-15V
$Comp
L CONN_01X03 P1
U 1 1 565E7A0F
P 1400 3400
F 0 "P1" H 1400 3600 50  0000 C CNN
F 1 "INPUT" V 1500 3400 50  0000 C CNN
F 2 "Connect:bornier3" H 1400 3400 50  0001 C CNN
F 3 "" H 1400 3400 50  0000 C CNN
	1    1400 3400
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X03 P2
U 1 1 565E7ABE
P 10300 3400
F 0 "P2" H 10300 3600 50  0000 C CNN
F 1 "OUTPUT" V 10400 3400 50  0000 C CNN
F 2 "Connect:bornier3" H 10300 3400 50  0001 C CNN
F 3 "" H 10300 3400 50  0000 C CNN
	1    10300 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	10100 1700 10100 3300
Wire Wire Line
	10100 4000 10100 3500
$Comp
L GND #PWR04
U 1 1 565E87DA
P 9850 3550
F 0 "#PWR04" H 9850 3300 50  0001 C CNN
F 1 "GND" H 9850 3400 50  0000 C CNN
F 2 "" H 9850 3550 50  0000 C CNN
F 3 "" H 9850 3550 50  0000 C CNN
	1    9850 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 3400 10100 3400
Wire Wire Line
	9850 3400 9850 3550
Wire Wire Line
	8400 5250 3350 5250
Wire Wire Line
	1600 3500 3350 3500
Wire Wire Line
	3750 3900 3350 3900
Wire Wire Line
	2950 1600 2950 3300
Wire Wire Line
	2950 3300 1600 3300
Wire Wire Line
	3300 2950 8350 2950
Connection ~ 9850 3400
$EndSCHEMATC
